<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Atividade Proposta de SASS</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav id="menuTopo">
                    <ul>
                        <li><a href="#">Home</a></li>                    
                        <li><a href="#">Empresa</a></li>                    
                        <li><a href="#">Produtos</a></li>                    
                        <li><a href="#">Serviços</a></li>                    
                        <li><a href="#">Contato</a></li>                    
                    </ul>
                </nav>
            </div><!--/.col -->
        </div><!--/.row -->

            <div class="row">
                <div class="col text-center">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum soluta dicta sint qui necessitatibus, consectetur expedita.</p>
                    <a href="#" class="botao-primario">Cadastrar</a>
                    <a href="#" class="botao-secundario">Editar</a>
                </div>
            </div><!--/.row -->

    </div><!--/.container -->

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>

</body>
</html>